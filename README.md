# MMorph3 ELG Wrapper

This is a simple [Spring Boot](https://spring.io/projects/spring-boot) based REST service that wraps the MMorph3 morphological analyzer functionality with the [ELG API](https://gitlab.com/european-language-grid/platform/elg-apis).

The service accepts a JSON [TextRequest](https://gitlab.com/european-language-grid/platform/elg-apis/blob/master/doc/T2.5-api-design.md#text-requests) on port 8080 and returns a JSON [AnnotationsResponse](https://gitlab.com/european-language-grid/platform/elg-apis/blob/master/doc/T2.5-api-design.md#annotations-response) containing the text token's morphological annotation. The following languages are supported: `de`, `en`, `es`, `fr`, `it`, `nl`. For each language, a separate endpoint is provided.

The service can be tested using the provided example query `text-request.json` with the following curl command:
```
curl -H "Content-Type: application/json" -d @text-request.json http://localhost:8080/en
```

The content of `text-request.json`, containing the text content to tokenize and annotate:
```
{
  "type": "text",
  "content": "Time flies like an arrow."
}
```

The corresponding JSON annotations response with the text token's morphological annotation:
```
{
  "response": {
    "type": "annotations",
    "annotations": {
      "morph": [{
          "start": 0,
          "end": 4,
          "features": {
            "token_morph": [{
                "POS": "noun",
                "INFL": {
                  "GENDER_NOUN": "neutrum",
                  "NUMBER_NOUN": "singular"
                },
                "STEM": "time",
                "SURFACE": "Time"
              }, {
                "POS": "verb",
                "INFL": {
                  "SUBTYPE_VERB": "main",
                  "VFORM_VERB": "bse"
                },
                "STEM": "time",
                "SURFACE": "Time"
              }
            ]
          }
        }, {
          "start": 5,
          "end": 10,
          "features": {
            "token_morph": [{
                "POS": "noun",
                "INFL": {
                  "GENDER_NOUN": "neutrum",
                  "NUMBER_NOUN": "plural"
                },
                "STEM": "fly",
                "SURFACE": "flies"
              }, {
                "POS": "verb",
                "INFL": {
                  "NUMBER_VERB": "singular",
                  "PERSON_VERB": "p3",
                  "SUBTYPE_VERB": "main",
                  "TENSE_VERB": "present",
                  "VFORM_VERB": "fin"
                },
                "STEM": "fly",
                "SURFACE": "flies"
              }
            ]
          }
        }, {
          "start": 11,
          "end": 15,
          "features": {
            "token_morph": [{
                "POS": "adjective",
                "INFL": {
                  "DEGREE_ADJECTIVE": "pos"
                },
                "STEM": "like",
                "SURFACE": "like"
              }, {
                "POS": "adverb",
                "INFL": {
                  "DEGREE_ADVERB": "pos",
                  "FN_ADVERB": "mod",
                  "WH_ADVERB": "no"
                },
                "STEM": "like",
                "SURFACE": "like"
              }, {
                "POS": "noun",
                "INFL": {
                  "GENDER_NOUN": "neutrum",
                  "NUMBER_NOUN": "singular"
                },
                "STEM": "like",
                "SURFACE": "like"
              }, {
                "POS": "verb",
                "INFL": {
                  "SUBTYPE_VERB": "main",
                  "VFORM_VERB": "bse"
                },
                "STEM": "like",
                "SURFACE": "like"
              }, {
                "POS": "part",
                "INFL": {
                  "SUBTYPE_PART": "prep"
                },
                "STEM": "like",
                "SURFACE": "like"
              }, {
                "POS": "adp",
                "INFL": {
                  "POSITION_ADP": "pre"
                },
                "STEM": "like",
                "SURFACE": "like"
              }
            ]
          }
        }, {
          "start": 16,
          "end": 18,
          "features": {
            "token_morph": [{
                "POS": "det",
                "INFL": {
                  "NUMBER_DET": "singular",
                  "SUBTYPE_DET": "art-indef"
                },
                "STEM": "a",
                "SURFACE": "an"
              }
            ]
          }
        }, {
          "start": 19,
          "end": 24,
          "features": {
            "token_morph": [{
                "POS": "noun",
                "INFL": {
                  "GENDER_NOUN": "neutrum",
                  "NUMBER_NOUN": "singular"
                },
                "STEM": "arrow",
                "SURFACE": "arrow"
              }
            ]
          }
        }, {
          "start": 24,
          "end": 25,
          "features": {
            "token_morph": [{
                "SURFACE": "."
              }
            ]
          }
        }
      ]
    }
  }
}
```