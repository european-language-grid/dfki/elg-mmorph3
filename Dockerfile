FROM maven:3.6.3-ibmjava-8-alpine as buildstage

COPY pom.xml /tmp/
COPY src /tmp/src/
COPY local-repo /tmp/local-repo/
WORKDIR /tmp/
RUN mvn package


FROM java:8-jre-alpine as appstage

ARG user=elg
ARG group=elg
ARG uid=1001
ARG gid=1001
ARG app_home=/MMorph3
ARG version=1.0.0

RUN mkdir $app_home \
  && addgroup -g $gid $group \
  && chown $uid:$gid $app_home \
  && adduser -D -h "$app_home" -u $uid -G $group $user

COPY --from=buildstage --chown=$uid:$gid /tmp/target/elg-mmorph3-$version.jar $app_home

USER $user
WORKDIR $app_home
# ARG is not available in ENTRYPOINT, but ENV is
ENV version=$version
ENTRYPOINT java -jar elg-mmorph3-$version.jar

EXPOSE 8080
