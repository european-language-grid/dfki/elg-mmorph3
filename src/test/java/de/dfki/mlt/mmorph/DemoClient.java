package de.dfki.mlt.mmorph;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.elg.model.AnnotationObject;
import eu.elg.model.requests.StructuredTextRequest;
import eu.elg.model.requests.StructuredTextRequest.Text;
import eu.elg.model.responses.AnnotationsResponse;


/**
 * Simple demo client that sends a {@link eu.elg.model.requests.StructuredTextRequest} with tokens
 * to the MMorph3 ELG wrapper and gets back an {@link eu.elg.model.responses.AnnotationsResponse}
 * with morphological annotation of the tokens.
 *
 * @author Jörg Steffen, DFKI
 */
public class DemoClient {

  private static final Logger logger = LoggerFactory.getLogger(DemoClient.class);


  /**
   * The main method.
   *
   * @param args
   *          the arguments; not used here
   */
  public static void main(String[] args) {

    try {
      ObjectMapper objectMapper = new ObjectMapper();

      // init structured text request
      StructuredTextRequest structuredTextRequest = new StructuredTextRequest();
      List<String> tokens =
          Arrays.asList(new String[] { "Time", "flies", "like", "an", "arrow", "." });
      // add tokens as texts to structured text request
      List<Text> texts = new ArrayList<Text>();
      for (String oneToken : tokens) {
        texts.add(new Text().withContent(oneToken));
      }
      structuredTextRequest.setTexts(texts);
      logger.info(String.format("structured text request as JSON:%n%s%n",
          objectMapper.writeValueAsString(structuredTextRequest)));

      // send structured text request to ELG MMorph wrapper
      HttpClient httpClient = HttpClients.createDefault();
      StringEntity requestEntity =
          new StringEntity(
              objectMapper.writeValueAsString(structuredTextRequest), ContentType.APPLICATION_JSON);
      URI uri = new URI(
          "http",
          null,
          "localhost", 8080, "/en",
          null,
          null);
      HttpPost postMethod = new HttpPost(uri);
      postMethod.setEntity(requestEntity);

      HttpResponse response = httpClient.execute(postMethod);
      int status = response.getStatusLine().getStatusCode();
      if (status >= 200 && status < 300) {
        HttpEntity entity = response.getEntity();
        if (entity != null) {
          String jsonResultString = EntityUtils.toString(entity);
          // the actual annotations response is nested in 'response'
          JsonNode resultJsonNode = objectMapper.readTree(jsonResultString);
          JsonNode annoString = resultJsonNode.get("response");
          logger.info(String.format("annotations response as JSON:%n%s%n", annoString));
          // unmarshall to annotations response
          AnnotationsResponse annotationResponse =
              objectMapper.treeToValue(annoString, AnnotationsResponse.class);
          // print token morphology
          List<AnnotationObject> morph = annotationResponse.getAnnotations().get("morph");
          logger.info("morph:");
          for (AnnotationObject oneMorph : morph) {
            logger.info(String.format(
                "%ntoken: %s%nmorphology: %s",
                tokens.get(oneMorph.getStart().intValue()),
                oneMorph.getFeatures().get("token_morph")));
          }
        }
      }

    } catch (URISyntaxException | IOException e) {
      logger.error(e.getLocalizedMessage(), e);
    }
  }
}
