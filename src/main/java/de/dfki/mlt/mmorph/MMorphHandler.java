package de.dfki.mlt.mmorph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.dfki.lt.sprout.morphology.json.Lexicon;
import de.dfki.lt.tools.tokenizer.JTok;
import de.dfki.lt.tools.tokenizer.annotate.AnnotatedString;
import de.dfki.lt.tools.tokenizer.output.Outputter;
import de.dfki.lt.tools.tokenizer.output.Token;
import eu.elg.handler.ElgHandler;
import eu.elg.handler.ElgHandlerRegistrar;
import eu.elg.handler.ElgHandlerRegistration;
import eu.elg.model.AnnotationObject;
import eu.elg.model.Response;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;

/**
 * Handler for MMorph requests.
 *
 * @author Jörg Steffen, DFKI
 */
@Component
@ElgHandler
public class MMorphHandler implements ElgHandlerRegistration {

  private ObjectMapper objectMapper;

  private JTok tokenizer;

  // map of language to corresponding morphology lexicon
  private Map<String, Lexicon> lexicons;


  /**
   * Create a new handler instance.
   *
   * @throws IOException
   *           if JTok initialization fails
   */
  @Autowired
  public MMorphHandler()
      throws IOException {

    this.objectMapper = new ObjectMapper();

    this.tokenizer = new JTok();

    this.lexicons = new HashMap<>();
    Lexicon lexiconDe = new Lexicon();
    lexiconDe.readFromClasspath("/lexicons/de_morph.slx");
    this.lexicons.put("de", lexiconDe);
    Lexicon lexiconEn = new Lexicon();
    lexiconEn.readFromClasspath("/lexicons/en_morph.slx");
    this.lexicons.put("en", lexiconEn);
    Lexicon lexiconEs = new Lexicon();
    lexiconEs.readFromClasspath("/lexicons/es_morph.slx");
    this.lexicons.put("es", lexiconEs);
    Lexicon lexiconFr = new Lexicon();
    lexiconFr.readFromClasspath("/lexicons/fr_morph.slx");
    this.lexicons.put("fr", lexiconFr);
    Lexicon lexiconIt = new Lexicon();
    lexiconIt.readFromClasspath("/lexicons/it_morph.slx");
    this.lexicons.put("it", lexiconIt);
    Lexicon lexiconNl = new Lexicon();
    lexiconNl.readFromClasspath("/lexicons/nl_morph.slx");
    this.lexicons.put("nl", lexiconNl);
  }


  /**
   * Process request for morphological annotation.
   *
   * @param request
   *          the request holding the text to tokenize and annotate
   * @param lang
   *          the language
   * @return annotation response with morphology annotation
   * @throws Exception
   *           if an error occurs
   */
  public Response<?> process(TextRequest request, String lang)
      throws Exception {

    Lexicon currentLexicon = this.lexicons.get(lang.toLowerCase());
    List<AnnotationObject> morphAnno = new ArrayList<>();

    // use JTok to tokenize text
    String inputText = request.getContent();
    AnnotatedString annotatedString = this.tokenizer.tokenize(inputText, lang);
    for (Token oneToken : Outputter.createTokens(annotatedString)) {
      String tokenSurface = oneToken.getImage();
      // get morphological analysis for token
      List<ObjectNode> singleTokenMorphAnno = currentLexicon.search(tokenSurface);
      if (singleTokenMorphAnno == null) {
        singleTokenMorphAnno = new ArrayList<>();
        singleTokenMorphAnno.add(
            this.objectMapper.createObjectNode().put(Lexicon.SURFACE_DEF, tokenSurface));
      }
      morphAnno.add(
          new AnnotationObject().withOffsets(oneToken.getStartIndex(), oneToken.getEndIndex())
          .withFeature("token_morph", singleTokenMorphAnno));
    }

    AnnotationsResponse response = new AnnotationsResponse();
    response.withAnnotations("morph", morphAnno);

    return response;
  }


  @Override
  public void registerHandlers(ElgHandlerRegistrar registrar) {

    registrar.handler("/de", TextRequest.class, r -> process(r, "de"));
    registrar.handler("/en", TextRequest.class, r -> process(r, "en"));
    registrar.handler("/es", TextRequest.class, r -> process(r, "es"));
    registrar.handler("/fr", TextRequest.class, r -> process(r, "fr"));
    registrar.handler("/it", TextRequest.class, r -> process(r, "it"));
    registrar.handler("/nl", TextRequest.class, r -> process(r, "nl"));
  }
}
