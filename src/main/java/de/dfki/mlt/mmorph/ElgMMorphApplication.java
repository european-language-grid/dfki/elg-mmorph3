package de.dfki.mlt.mmorph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The top level Spring Boot application.
 *
 * @author Jörg Steffen, DFKI
 */
@SpringBootApplication
public class ElgMMorphApplication {

  /**
   * Application entry method.
   *
   * @param args
   *          the arguments
   */
  public static void main(String[] args) {

    SpringApplication.run(ElgMMorphApplication.class, args);
  }
}
